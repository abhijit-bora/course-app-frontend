import React, { useState } from "react";
import { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";
import AllCoursesPage from "./components/AllCoursesPage";
import "./App.css";
import AuthForm from "./components/AuthForm";

import TodayContentC from "./components/TodayContentC";
import Slider from "./Slider/Slider";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={AuthForm} />
        <Route exact path="/allcourse" component={AllCoursesPage} />
        <Route exact path="/morecontent/:id" component={Slider} />
        <Route exact path="/s" component={Slider} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
