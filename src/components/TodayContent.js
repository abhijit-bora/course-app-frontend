// import React, { useState } from "react";
// import { useHistory, useParams } from "react-router-dom";
// import {
//   Card,
//   Icon,
//   Button,
//   Header,
//   Image,
//   Modal,
//   Segment,
// } from "semantic-ui-react";
// import AllCoursesPage from "./AllCoursesPage";
// import "../App.css";
// import CardMedia from "@mui/material/CardMedia";
// import Data from "./data";

// function TodayContent({ setIsLoggedIn }) {
//   const history = useHistory();

//   let { id } = useParams();
//   async function loggedOut() {
//     history.push("/login");
//   }

//   async function back() {
//     history.push("/allcourse");
//   }

//   let image = Data.data[id].image;
//   let t = Data.data[id].title;
//   console.log(id);
//   let date = 2;

//   return (
//     <>
//       <div className="navbar">
//         <Button
//           labelPosition="left"
//           icon="left chevron"
//           content="Back"
//           onClick={back}
//         />
//         <h2 className="ui left floated header">Today's Content</h2>
//         <h2 className="ui right floated header">
//           <Button onClick={loggedOut} className="ui button">
//             Log Out
//           </Button>
//         </h2>
//       </div>
//       <div className="grid-col">
//         {Data.data[id].topics.map((course, i) => {
//           if (i < date * 4 && i >= (date - 1) * 4) {
//             return (
//               <Card className="grid-item" key={i}>
//                 <CardMedia component="img" height="140" image={image} />
//                 <Card.Content header={t} />
//                 <Card.Content description={course} />
//               </Card>
//             );
//           }
//         })}
//       </div>
//     </>
//   );
// }

// export default TodayContent;
