export default {
  data: [
    {
      id: "c1",
      title: "React JS",
      image:
        "https://blog.addthiscdn.com/wp-content/uploads/2014/11/addthis-react-flux-javascript-scaling.png",
      description:
        "React. js is an open-source JavaScript library that is used for building user interfaces specifically for single-page applications.",
      topics: [
        "topic1",
        "topic2",
        "topic3",
        "topic4",
        "topic5",
        "topic6",
        "topic7",
        "topic8",
        "topic9",
        "topic10",
        "topic12",
        "topic13",
      ],
    },
    {
      id: "c2",
      title: "Node JS",
      image: "https://www.the-guild.dev/blog-assets/nodejs-esm/nodejs_logo.png",
      description:
        "Node. js is primarily used for non-blocking, event-driven servers, due to its single-threaded nature",
      topics: [
        "topic1",
        "topic2",
        "topic3",
        "topic4",
        "topic5",
        "topic6",
        "topic7",
        "topic8",
        "topic9",
        "topic10",
        "topic12",
        "topic13",
      ],
    },
    {
      id: "c3",
      title: "C++",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/800px-ISO_C%2B%2B_Logo.svg.png",
      description:
        "Like C, it is used when a low-level programming language is necessary. While C++ is commonly used for graphics-heavy software ",
      topics: [
        "topic1",
        "topic2",
        "topic3",
        "topic4",
        "topic5",
        "topic6",
        "topic7",
        "topic8",
        "topic9",
        "topic10",
        "topic12",
        "topic13",
      ],
    },
    {
      id: "c4",
      title: "Java",
      image:
        "https://www.gcreddy.com/wp-content/uploads/2021/06/Java-Programming-Syllabus-1536x860.png",
      description:
        "One of the most widely used programming languages, Java is used as the server-side language for most back-end development projects, including those involving big data and Android development",
      topics: [
        "topic1",
        "topic2",
        "topic3",
        "topic4",
        "topic5",
        "topic6",
        "topic7",
        "topic8",
        "topic9",
        "topic10",
        "topic12",
        "topic13",
      ],
    },
    {
      id: "c5",
      title: "Python",
      image:
        "https://ictslab.com/wp-content/uploads/2019/03/d1326ca6cca8038cd115a061b4e2b3bc.png",
      description:
        "Python is a computer programming language often used to build websites and software, automate tasks, and conduct data analysis. ",
      topics: [
        "topic1",
        "topic2",
        "topic3",
        "topic4",
        "topic5",
        "topic6",
        "topic7",
        "topic8",
        "topic9",
        "topic10",
        "topic12",
        "topic13",
      ],
    },
    {
      id: "c6",
      title: "C++",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/800px-ISO_C%2B%2B_Logo.svg.png",
      description:
        "Like C, it is used when a low-level programming language is necessary. While C++ is commonly used for graphics-heavy software ",
      topics: [
        "topic1",
        "topic2",
        "topic3",
        "topic4",
        "topic5",
        "topic6",
        "topic7",
        "topic8",
        "topic9",
        "topic10",
        "topic12",
        "topic13",
      ],
    },
    {
      id: "c7",
      title: "React JS",
      image:
        "https://blog.addthiscdn.com/wp-content/uploads/2014/11/addthis-react-flux-javascript-scaling.png",
      description:
        "React. js is an open-source JavaScript library that is used for building user interfaces specifically for single-page applications.",
      topics: [
        "topic1",
        "topic2",
        "topic3",
        "topic4",
        "topic5",
        "topic6",
        "topic7",
        "topic8",
        "topic9",
        "topic10",
        "topic12",
        "topic13",
      ],
    },
    {
      id: "c8",
      title: "Node JS",
      image: "https://www.the-guild.dev/blog-assets/nodejs-esm/nodejs_logo.png",
      description:
        "Node. js is primarily used for non-blocking, event-driven servers, due to its single-threaded nature",
      topics: [
        "topic1",
        "topic2",
        "topic3",
        "topic4",
        "topic5",
        "topic6",
        "topic7",
        "topic8",
        "topic9",
        "topic10",
        "topic12",
        "topic13",
      ],
    },
    {
      id: "c9",
      title: "C++",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/800px-ISO_C%2B%2B_Logo.svg.png",
      description:
        "Like C, it is used when a low-level programming language is necessary. While C++ is commonly used for graphics-heavy software ",
      topics: [
        "topic1",
        "topic2",
        "topic3",
        "topic4",
        "topic5",
        "topic6",
        "topic7",
        "topic8",
        "topic9",
        "topic10",
        "topic12",
        "topic13",
      ],
    },
    {
      id: "c10",
      title: "React JS",
      image:
        "https://blog.addthiscdn.com/wp-content/uploads/2014/11/addthis-react-flux-javascript-scaling.png",
      description:
        "React. js is an open-source JavaScript library that is used for building user interfaces specifically for single-page applications.",
      topics: ["topic1", "topic2", "topic3"],
    },
    {
      id: "c11",
      title: "Node JS",
      image: "https://www.the-guild.dev/blog-assets/nodejs-esm/nodejs_logo.png",
      description:
        "Node. js is primarily used for non-blocking, event-driven servers, due to its single-threaded nature",
      topics: ["topic1", "topic2", "topic3"],
    },
    {
      id: "c12",
      title: "C++",
      image:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/800px-ISO_C%2B%2B_Logo.svg.png",
      description:
        "Like C, it is used when a low-level programming language is necessary. While C++ is commonly used for graphics-heavy software ",
      topics: ["topic1", "topic2", "topic3"],
    },
  ],
};
