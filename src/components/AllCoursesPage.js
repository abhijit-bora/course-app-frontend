import React from "react";
import { useHistory } from "react-router-dom";
import {
  Card,
  Icon,
  Button,
  Header,
  Image,
  Modal,
  Segment,
} from "semantic-ui-react";
import CardMedia from "@mui/material/CardMedia";
import "../App";
import Data from "./data";
// const HeaderExampleFloating = () => (
//   <Segment clearing>
//     <Header as="h3" floated="right"></Header>
//     <Header as="h3" floated="left"></Header>
//   </Segment>
// );

function AllCoursesPage() {
  //   const [modalData, setModalData] = React.useState([]);
  //   const [open, setOpen] = React.useState(false);
  const history = useHistory();
  async function readmore(id) {
    history.push(`/morecontent/${id}`);
  }
  async function loggedOut() {
    history.push("/");
  }
  return (
    // console.log(dummy_data),
    <>
      <div className="navbar">
        <h2 className="ui right floated header">ALL COURSES</h2>
        <h2 class="ui left floated header">
          <Button onClick={loggedOut} className="ui button">
            Log Out
          </Button>
        </h2>
      </div>

      <div className="grid-container">
        {Data.data.map((course, i) => {
          return (
            <Card className="grid-item" key={i}>
              <CardMedia component="img" height="140" image={course.image} />
              <Card.Content header={course.title} />
              <Card.Content description={course.description} />
              <Card.Content extra>
                <Button
                  //   onClick={readmore}
                  onClick={() => {
                    //   setModalData(course.topics);
                    readmore(i);
                  }}
                  class="ui button"
                >
                  More Content
                </Button>
              </Card.Content>
            </Card>
          );
        })}
      </div>

      {/* { <Modal
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
        open={open}
      >
        <Modal.Header>Contents</Modal.Header>
        <Modal.Content>
          <div className="modalData">
            {modalData.map((course) => {
              return <ol>{course}</ol>;
            })}
          </div>
        </Modal.Content>
        <Modal.Actions>
          <Button color="black" onClick={() => setOpen(false)}>
            Close
          </Button>
        </Modal.Actions>
      </Modal> */}
    </>
  );
}
export default AllCoursesPage;
